﻿using UnityEngine;
using System.Collections;

public enum SCREENS
{
	SCREEN_HUD, //0
	SCREEN_WIN,  //1
}

public enum ENEMYS
{
	ENEMY_NORMAL,  //0
	ENEMY_KAMIKAZE,  //0
	ENEMY_BOSS,  //1
}