﻿using UnityEngine;
using System.Collections;

public class LevelManager : MonoBehaviour {

	private static LevelManager instance;
	public int maxNumeroEnemigos;
	public int maxNumeroParedes;
//	public int maxNumeroEnemigosPorArea;
//	public float radioArea=10;
	[HideInInspector]//Para ocultar la variable en el editor
	public int actualNumEnemigo;
	public GameObject enemigo;
	public GameObject wall;
	public Player playerActual;
	//Para crear las paredes de forma dinamica



	public GameObject Baul;
	public int maxNumeroBaules;


	void Awake()
	{
		//Si la instancia existe destruimos
		if (instance != null && instance != this)
		{
			Destroy(this.gameObject);
			return;
		}
		else
		{
			instance = this;
		}
	}

	public static LevelManager Instance
	{
		get
		{
			return instance;
		}
	}

	// Use this for initialization
	void Start () {
		CrearParedes ();
		CrearBaules ();
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}

//	public void CrearEnemigosEnArea(Vector3 puntoGuia)
//	{
//		for (int i=0; i<maxNumeroEnemigosPorArea; i++) 
//		{
//			Vector3 posicionRandom = UnityEngine.Random.insideUnitSphere;
//			Instantiate(enemigo[0], puntoGuia+posicionRandom,transform.rotation);
//
//		}
//	}

	public void CrearEnemigos()
	{
		//Creamos los enemigos y los Range son los limites donde van a salir los enemigos
		for (int i=0; i<maxNumeroEnemigos; i++) 
		{
			Instantiate(enemigo, new Vector3(Random.Range(-196,196),1,Random.Range(-112,200)),transform.rotation);
		}
	}

	public void CrearParedes()
	{
		//Creamos los enemigos y los Range son los limites donde van a salir los enemigos
		for (int i=0; i<maxNumeroParedes; i++) 
		{
			Vector3 euler = transform.eulerAngles;
			//Aqui se controla como se quieren mostrar las paredes, en angulo de 90
			euler.y = i*90; //Random.Range (0f, 360f);

			GameObject go=Instantiate(wall, new Vector3(Random.Range(-196,196),-3,Random.Range(-112,200)),transform.rotation) as GameObject;
			go.transform.eulerAngles = euler;
		}
	}

	public void CrearBaules()
	{
		//Creamos los enemigos y los Range son los limites donde van a salir los enemigos
		for (int i=0; i<maxNumeroBaules; i++) 
		{
			Vector3 euler = transform.eulerAngles;
			//Aqui se controla como se quieren mostrar las paredes, en angulo de 90
			euler.y = Random.Range (0f, 360f);
			
			GameObject go=Instantiate(Baul, new Vector3(Random.Range(-136,136),12,Random.Range(-112,150)),transform.rotation) as GameObject;
			go.transform.eulerAngles = euler;
		}
	}


	//Cuando notifico que se destruye un enemigo actualizo el numero
	public void NotificacionDestruccionEnemigo(){

		actualNumEnemigo++;
		CheckWinConditions();
	}

	public void CheckWinConditions(){
	
//	if (maxNumeroBaules == 0)
//		{
//			Debug.Log("Ganaste");
//			HUDManager.Instance.ShowWinScreen();
//		}

	if(actualNumEnemigo>maxNumeroEnemigos){
			//Todo ganar
		Debug.Log("Ganaste");
	HUDManager.Instance.ShowWinScreen();
		}
	}

	public Player GetActualPlayer(){
		return playerActual;
	}

}
