﻿using UnityEngine;
using System.Collections;

public class Enemigo : EntityBase {

	public ENEMYS type;
	public GameObject target; //para saber a quien tiene que atacar, lo dejamos publico si queremos que el 
								//comportamiento del enemigo, no solo vaya dirigido hacia el player. Tambien para que cuide los baules.
	public float speed; // la velocidad del enemigo;
	public bool moverse;
	private NavMeshAgent agent;
	public GameObject bala;
	private Transform myTransform;

	// Use this for initialization
	void Start () {
		myTransform=transform;
		//Que el Player actual
		target = LevelManager.Instance.GetActualPlayer().gameObject;
		agent = gameObject.GetComponent<NavMeshAgent>();
		//invoque la funcion que dispara en 2 segundas y que la vuelva a cargar cada 5 segundos
		InvokeRepeating ("Disparar",Random.Range(1,2), Random.Range(5,8));

	
	}

	public void Disparar()
	{
		GameObject go = Instantiate(bala, myTransform.position+myTransform.forward, myTransform.rotation) as GameObject;
		go.GetComponent<Proyectil>().parent=this;
	}
	
	// Update is called once per frame
	void Update () 
	{
		//para poner a funcionar que los enemigos no traspasen las paredes
		agent.SetDestination(target.transform.position);
	}

	void ManualMovement()
	{
		if (Vector3.SqrMagnitude(target.transform.position - transform.position) < 15500)
		{
			//Debug.Log("Esta cerca");
			moverse=true;
		}
		if(moverse) 
		{
			//Para que llegue hasta cierta distancia del jugador sin que lo toque, calcular distancia
			//El SQRmagnitud 
			if(Vector3.SqrMagnitude(target.transform.position - transform.position) > 8) 
			{
			
				// que el enemigo mire al personaje
				transform.rotation = Quaternion.LookRotation (target.transform.position - transform.position);
				//Que el enemigo se dirija hacia el player
				transform.position += transform.forward * speed * Time.deltaTime;
			}
		}
		if (Vector3.SqrMagnitude(target.transform.position - transform.position) > 800)
		{
			moverse=false;
		}


	}

	public void Destruir(){
		Debug.Log ("Colision con enemigo");
		Destroy(gameObject);
	}
}
