﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;



public class Player : EntityBase {

	public float speed;    
	public float rotationSpeed;
	public EntityStats stats;
	private Transform myTransform;
	public float puntajeFinal;

	//public int puntaje;
	//public Text textoPuntaje;


	//Todo lo que no tenga un scrip referenciado es un GameObject
	public GameObject bala;

	// Use this for initialization
	void Start () {
		myTransform = transform;
		LevelManager.Instance.CrearEnemigos();
		stats = gameObject.GetComponent<EntityStats>();

	}

	void Update(){
		PlayerInput ();
	}
	
	// Update is called once per frame

	void PlayerInput () 

	{
		if(Input.GetKey(KeyCode.W)){
			myTransform.position += myTransform.forward * speed * Time.deltaTime;
		
		}
		if(Input.GetKey(KeyCode.S)){
			myTransform.position -= myTransform.forward * speed * Time.deltaTime;
			
		}
		if(Input.GetKey(KeyCode.A)){
			myTransform.eulerAngles -= new Vector3(0,rotationSpeed*Time.deltaTime,0);
			
		}
		if(Input.GetKey(KeyCode.D)){
			myTransform.eulerAngles += new Vector3(0,rotationSpeed*Time.deltaTime,0);
			
		}
		//Aqui le estamos aplicando la fuerza al rigidbody(De esta forma podemos acceder a los componentes que tiene el objeto en el inspector)
		if(Input.GetKeyDown(KeyCode.Space)){
			gameObject.GetComponent<Rigidbody>().AddForce(new Vector3(0,500,0));
			
		}

		if (Input.GetMouseButtonDown (0)) {
			//Que la bala se cree en mi posicion pero un poco mas adelante, forward es un vetor unitario donde los rangos van a estar entre 1 y menos 1
			GameObject go = Instantiate(bala, myTransform.position+myTransform.forward, myTransform.rotation) as GameObject;
			go.GetComponent<Proyectil>().parent=this;
		}

	}



	public void PausarJuego()
	{
		Time.timeScale = 0;

	}
	public void DesPausarJuego()
	{
		Time.timeScale = 1;
		
	}

	//Cuando hay una colision entre dos eventos, estoy tres eventos son nativos de unity
	//Si choca se destruye el baul
	void OnTriggerEnter(Collider other)
	{
		if(other.tag.Equals("BaulCoins"))

		{
			stats.puntaje += other.gameObject.GetComponent<Coins>().puntos;
			HUDManager.Instance.ActualizarPuntaje(stats.puntaje);


			/*if(stats!=null)
			{
				stats.ActualizarPuntaje(other.gameObject.GetComponent<Coins>().puntos);
			}*/
			//Podemos buscar cualquier gameObject que este en la escena por nombre
			/*if(GameObject.FindObjectOfType<HUDManager>()!=null)
			{
				GameObject.FindObjectOfType<HUDManager>().GetComponent<HUDManager>().ActualizarPuntaje(stats.puntaje);
			}*/
			//HUDManager.Instance.ActualizarPuntaje(puntaje);
			Destroy(other.gameObject);
		}
		if (stats.puntaje > puntajeFinal) 
		{
			Debug.Log("Ganaste");
			HUDManager.Instance.ShowWinScreen();
		}
	}

	public void DestruyoEnemigo(){
	
		LevelManager.Instance.NotificacionDestruccionEnemigo();
	}

	//El daño que me hacen
	public void Hitted(int damage)
	{

	}

	void OnTriggerStay(Collider other)
	{
		
	}
	void OnTriggerExit(Collider other)
	{
		
	}
}