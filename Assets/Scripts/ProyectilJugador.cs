﻿using UnityEngine;
using System.Collections;

public class ProyectilJugador:Player
{

	public Player parent;

	void OnCollisionEnter(Collision other)
	{
		if(other.gameObject.tag.Equals("Enemigo"))
			
		{	
			Debug.Log("Hacer daño al Enemigo");
			other.gameObject.GetComponent<Enemigo>().Destruir();		
		if(parent != null)
			{
			//Debug.Log("Le pego al enemigo");
				parent.DestruyoEnemigo();
			}
			
		}
	}
}
