﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HUDManager : MonoBehaviour {

	public Text textoPuntaje;
	//Las pantallas son screenBase y vamos a tener muchas pantallas
	public ScreenBase[] screens;

	private static HUDManager instance;


	void Awake()
	{
		if (instance != null && instance != this)
		{
			Destroy(this.gameObject);
			return;
		}
		else
		{
			instance = this;
		}
	}
	
	public static HUDManager Instance
	{
		get
		{
			return instance;
		}
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void ActualizarPuntaje(int puntaje){
		//Debug.Log ("Notificar");
		((ScreenHud)screens[(int)SCREENS.SCREEN_HUD]).ActualizarPuntaje(puntaje);
	}

	public void ShowWinScreen(){
		//para activar o desactivar pantalla
		screens [(int)SCREENS.SCREEN_WIN].ShowScreen ();
	}
}
