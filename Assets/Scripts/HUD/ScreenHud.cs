﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScreenHud : ScreenBase {

	public Text textoPuntaje;

	// Use this for initialization
	public override void Start () {
		base.Start ();
		ShowScreen ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public override void ShowScreen(){
		iTween.MoveTo (gameObject, iTween.Hash ("y", 0, "time", 1));
	}

	public void ActualizarPuntaje(int puntaje){
		textoPuntaje.text= "Puntaje: "+puntaje;
	}
}
