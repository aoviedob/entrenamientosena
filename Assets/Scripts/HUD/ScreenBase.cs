﻿using UnityEngine;
using System.Collections;

public class ScreenBase : EntityBase {

	public SCREENS type;

	// Use this for initialization
	public virtual void Start () {

	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public virtual void ShowScreen(){
		iTween.MoveTo (gameObject, iTween.Hash ("x", 0, "time", 1));
	}

	public void HideScreen(){
	}
}
