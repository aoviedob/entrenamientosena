﻿using UnityEngine;
using System.Collections;

public class Proyectil:EntityBase {

	public float speed;
	public float tiempoParaDesaparecer;
	public EntityBase parent;



	// Use this for initialization
	void Start () {

		Invoke ("DestruirBala", tiempoParaDesaparecer);
	
	}
	
	// Update is called once per frame
	void Update () {
		transform.position += transform.forward * speed * Time.deltaTime;
	
	}

	void DestruirBala(){


			//gameObject en minuscula es el objeto que esta en el mundo
			//Pero con esto se destruye la bala de referencia y no podemos seguir disparando
			Destroy (gameObject);
	

	}


}
